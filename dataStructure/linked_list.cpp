#include <iostream>
using namespace std;

class Node {
 public:
  int val;
  Node* next;
  Node(int x) {
    val = x;
    next = nullptr;
  }
};

class MyLinkedList {
 private:
  Node* head = nullptr;
  int size = 0;

 public:
  MyLinkedList() {}

  int get(int index) {
    if (size <= index) return -1;
    Node* cur = head;
    for (int i = 0; i < index; i++) {
      cur = cur->next;
    }
    return cur->val;
  }

  void addAtHead(int val) {
    Node* temp = head;
    head = new Node(val);
    head->next = temp;
    size++;
  }

  void addAtTail(int val) {
    if (head == nullptr) {
      return addAtHead(val);
    }
    Node* temp = head;
    while (temp->next != nullptr) temp = temp->next;
    temp->next = new Node(val);
    size++;
  }

  void addAtIndex(int index, int val) {
    if (size < index) return;
    if (index == 0) return addAtHead(val);
    Node* pre = head;
    for (int i = 0; i < index - 1; i++) pre = pre->next;
    Node* post = pre->next;
    pre->next = new Node(val);
    pre->next->next = post;
    size++;
  }

  void deleteAtIndex(int index) {
    if (size <= index) return;
    if (index == 0) {
      Node* to_delete = head;
      head = head->next;
      to_delete->next = nullptr;
      size--;
      delete to_delete;
      return;
    }
    Node* pre = head;
    for (int i = 0; i < index - 1; i++) pre = pre->next;
    Node* to_delete = pre->next;
    pre->next = to_delete->next;
    to_delete->next = nullptr;
    size--;
    delete to_delete;
  }
};

int main() {
  MyLinkedList* obj = new MyLinkedList();
  return 0;
}
