#include <string.h>

#include <iostream>
using namespace std;

// delete vs delete[],
// https://stackoverflow.com/questions/2425728/delete-vs-delete-operators-in-c
// char* vs char[],
// https://www.quora.com/What-is-the-difference-between-char*-and-char

class Marine {
  int hp;
  int x_cord, y_cord;
  int damage;
  bool is_dead;
  char *name;

 public:
  Marine();
  Marine(int x, int y);
  Marine(int x, int y, const char *new_name);
  ~Marine();

  int Attack();
  void GetAttacked(int damage_get);
  void Move(int x, int y);

  void ShowStat();
};

Marine::Marine() {
  hp = 50;
  x_cord = y_cord = 0;
  damage = 5;
  is_dead = false;
  name = NULL;
}

Marine::Marine(int x, int y) {
  x_cord = x;
  y_cord = y;
  hp = 50;
  damage = 5;
  is_dead = false;
  name = NULL;
}

Marine::Marine(int x, int y, const char *new_name) {
  x_cord = x;
  y_cord = y;
  hp = 50;
  damage = 5;
  is_dead = false;
  name = new char[strlen(new_name) + 1];
  strcpy(name, new_name);
}

Marine::~Marine() {
  cout << "delete " << name << endl;

  if (name != NULL) {
    delete[] name;
  }
}

void Marine::Move(int x, int y) {
  x_cord = x;
  y_cord = y;
}

int Marine::Attack() { return damage; }

void Marine::GetAttacked(int damage_get) {
  hp -= damage_get;
  if (hp <= 0) {
    hp = 0;
    is_dead = true;
  }
}

void Marine::ShowStat() {
  cout << "** Marine: " << name << " **" << endl;
  cout << "Location: " << x_cord << ", " << y_cord << endl;
  cout << "HP: " << hp << endl;
}

int main() {
  Marine *marines[100];

  marines[0] = new Marine(2, 3, "James");
  marines[1] = new Marine(3, 5, "Ralf");

  marines[0]->ShowStat();
  marines[1]->ShowStat();

  cout << endl << "1 attacks 2" << endl;
  marines[1]->GetAttacked(marines[0]->Attack());

  marines[0]->ShowStat();
  marines[1]->ShowStat();

  delete marines[0];
  delete marines[1];

  return 0;
}