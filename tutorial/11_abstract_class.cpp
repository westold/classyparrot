#include <iostream>
using namespace std;

class Animal{
  public:
  Animal(){}
  virtual ~Animal(){}
  virtual void Speak() = 0;
};

class Dog: public Animal{
  public:
  Dog():Animal(){}
  void Speak() override {cout << "Bow! Wow!"<< endl;}
};

class Cat: public Animal{
  public:
  Cat():Animal(){}
  void Speak() override {cout << "Meow!" << endl;}
};

int main(){
  Animal* dog = new Dog();
  Animal* cat = new Cat();

  //error!
  //Animal* animal = new Animal();

  dog->Speak();
  cat->Speak();
}
