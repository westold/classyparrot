#include <iostream>
using namespace std;

int main() {
  int arr_size;

  cout << "array size: ";
  cin >> arr_size;

  int *list = new int[arr_size];

  for (int i = 0; i < arr_size; i++) {
    cout << "set " << i + 1 << "th elemnet: ";
    cin >> list[i];
  }

  for (int i = 0; i < arr_size; i++) {
    cout << "the " << i + 1 << "th element is: " << list[i] << endl;
  }

  delete[] list;

  return 0;
}
