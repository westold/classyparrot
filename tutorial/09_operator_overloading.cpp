#include <iostream>

using namespace std;

class MyArray;
class Iterable;

class MyArray {
  friend Iterable;

 private:
  const int dim_;
  int *size_;  // array size is "size[0] * size[1] * ... * size[dim-1]"

  struct Address {
    int level;
    void *child;
  };
  Address *root_;

  void InitAddress(Address *current) {
    if (!current) return;

    if (current->level == dim_ - 1) {
      current->child = new int[size_[current->level]];
      return;
    }

    current->child = new Address[size_[current->level]];

    for (int i = 0; i < size_[current->level]; i++) {
      (static_cast<Address *>(current->child) + i)->level = current->level + 1;
      InitAddress(static_cast<Address *>(current->child) + i);
    }
  };

  void DeleteAddress(Address *current) {
    if (!current) return;

    for (int i = 0; current->level < dim_ - 1 && i < size_[current->level];
         i++) {
      DeleteAddress(static_cast<Address *>(current->child) + i);
    }

    // IMO, this is not good example for deleting void pointer.
    delete[] current->child;
  }

 public:
#pragma region Iterator
  class Iterator {
    friend Iterable;

   private:
    int *location_;
    MyArray *array_;

   public:
    Iterator(MyArray *array, int *location = nullptr) : array_(array) {
      location_ = new int[array_->dim_];
      for (int i = 0; i < array_->dim_; i++)
        location_[i] = (location != nullptr ? location[i] : 0);
    };

    Iterator(const Iterator &itr) : array_(itr.array_) {
      location_ = new int[array_->dim_];
      for (int i = 0; i < array_->dim_; i++) location_[i] = itr.location_[i];
    }

    ~Iterator() { delete[] location_; }

    Iterator &operator++() {
      if (location_[0] >= array_->size_[0]) return *this;

      bool is_going_up = false;
      int i = array_->dim_ - 1;
      do {
        location_[i]++;

        if (location_[i] >= array_->size_[i] && i >= 1) {
          location_[i] -= array_->size_[i];
          is_going_up = true;
          i--;
        } else
          is_going_up = false;

      } while (i >= 0 && is_going_up);

      return *this;
    }

    const Iterator operator++(int) {
      Iterator temp = *this;
      ++*this;
      return temp;
    }

    Iterator &operator=(const Iterator &itr) {
      array_ = itr.array_;
      location_ = new int[itr.array_->dim_];
      for (int i = 0; i < array_->dim_; i++) location_[i] = itr.location_[i];

      return *this;
    }

    bool operator!=(const Iterator &itr) {
      if (array_->dim_ != itr.array_->dim_) return true;

      for (int i = 0; i < array_->dim_; i++) {
        if (location_[i] != itr.location_[i]) return true;
      }

      return false;
    }

    Iterable operator*();
  };
#pragma endregion

#pragma region MyArray
  MyArray(int dim, int *size) : dim_(dim) {
    size_ = new int[dim];
    for (int i = 0; i < dim; i++) size_[i] = size[i];

    root_ = new Address;
    root_->level = 0;

    InitAddress(root_);
  }

  MyArray(const MyArray &array) : dim_(array.dim_) {
    size_ = new int[dim_];
    for (int i = 0; i < dim_; i++) size_[i] = array.size_[i];

    root_ = new Address;
    root_->level = 0;

    InitAddress(root_);
  }

  Iterable operator[](const int index);
  Iterator First() {
    int *temp_array = new int[dim_];
    for (int i = 0; i < dim_; i++) temp_array[i] = 0;

    Iterator temp(this, temp_array);
    delete[] temp_array;

    return temp;
  }

  Iterator Last() {
    int *temp_array = new int[dim_];
    temp_array[0] = size_[0];

    for (int i = 1; i < dim_; i++) temp_array[i] = 0;

    Iterator temp(this, temp_array);
    delete[] temp_array;

    return temp;
  }

  ~MyArray() {
    DeleteAddress(root_);
    delete[] size_;
  }
#pragma endregion
};

class Iterable {
 private:
  void *data_;
  int level_;
  MyArray *array_;

 public:
#pragma region Constructor
  Iterable(int index, int level = 0, void *data = nullptr,
           MyArray *array = nullptr)
      : level_(level), data_(data), array_(array) {
    if (level < 1 || index >= array_->size_[level - 1]) {
      data_ = nullptr;
      return;
    }

    if (level_ == array_->dim_) {
      data_ =
          static_cast<int *>(static_cast<MyArray::Address *>(data_)->child) +
          index;
    } else {
      data_ = static_cast<MyArray::Address *>(
                  static_cast<MyArray::Address *>(data_)->child) +
              index;
    }
  }

  Iterable(const Iterable &i)
      : data_(i.data_), level_(i.level_), array_(i.array_) {}
#pragma endregion

  operator int() {
    if (data_) return *static_cast<int *>(data_);
    return 0;
  }

  Iterable &operator=(const int &num) {
    if (data_) *static_cast<int *>(data_) = num;
    return *this;
  }

  Iterable operator[](const int index) {
    if (!data_) return 0;
    return Iterable(index, level_ + 1, data_, array_);
  }
};

Iterable MyArray::operator[](const int index) {
  return Iterable(index, 1, static_cast<void *>(root_), this);
};

Iterable MyArray::Iterator::operator*() {
  Iterable iter = array_->operator[](location_[0]);

  for (int i = 1; i < array_->dim_; i++) {
    iter = iter.operator[](location_[i]);
  }

  return iter;
}

int main() {
  int size[] = {2, 3, 4};
  MyArray arr(3, size);

  MyArray::Iterator itr = arr.First();
  for (int i = 0; itr != arr.Last(); itr++, i++) (*itr) = i;
  for (itr = arr.First(); itr != arr.Last(); itr++)
    std::cout << *itr << std::endl;

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 3; j++) {
      for (int k = 0; k < 4; k++) {
        arr[i][j][k] = (i + 1) * (j + 1) * (k + 1);
      }
    }
  }

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 3; j++) {
      for (int k = 0; k < 4; k++) {
        std::cout << i << " " << j << " " << k << " " << arr[i][j][k]
                  << std::endl;
      }
    }
  }

  return 0;
}