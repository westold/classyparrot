#pragma once
#include <string>
using std::string;
namespace myexcel {
class Vector {
 public:
  Vector(int n = 1);
  ~Vector();
  void PushBack(string s);
  void RemoveAt(int index);
  string operator[](int i);
  int length();

 private:
  string* data_;
  int capacity_;
  int length_;
};

class Stack {
 public:
  Stack();
  ~Stack();
  void Push(string str);
  string Pop();
  string Peek();
  bool IsEmpty();

 private:
  struct Node {
   public:
    Node(Node* pre, string str) : pre_(pre), content_(str) {}
    Node* pre_;
    string content_;
  };

  Node* current_;
  Node start_;
};

class NumStack {
 public:
  NumStack();
  ~NumStack();
  void Push(double num);
  double Pop();
  double Peek();
  bool IsEmpty();

 private:
  struct Node {
   public:
    Node(Node* pre, double num) : pre_(pre), content_(num) {}
    Node* pre_;
    double content_;
  };
  Node* current_;
  Node start_;
};
}  // namespace myexcel
#pragma