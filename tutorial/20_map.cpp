#include <iostream>
#include <map>
#include <string>
using namespace std;

// set, map are sequence containers

template <typename K, typename V>
void PrintElements(map<K, V>& m) {
  // loop with iterator
  // for (auto itr = m.begin(); itr != m.end(); ++itr)
  //   cout << itr->first << ": " << itr->second << endl;
  for (const pair<K, V>& p : m) cout << p.first << ": " << p.second << endl;
}

template <typename K, typename V>
void SearchAndPrint(map<K, V>& m, K key) {
  auto itr = m.find(key);
  if (itr != m.end())
    cout << key << ": " << itr->second << endl;
  else
    cout << key << ": none" << endl;
}

template <typename K, typename V>
void PrintElements(multimap<K, V>& m) {
  // loop with iterator
  // for (auto itr = m.begin(); itr != m.end(); ++itr)
  //   cout << itr->first << ": " << itr->second << endl;
  for (const pair<K, V>& p : m) cout << p.first << ": " << p.second << endl;
}

int main() {
  // map
  map<string, double> scores_by_student;
  scores_by_student.insert(pair<string, double>("성삼문", 2.28));
  scores_by_student.insert(pair<string, double>("하위지", 3.42));
  // or we can use make_pair
  scores_by_student.insert(make_pair("이개", 3.05));
  scores_by_student.insert(make_pair("유성원", 4.11));
  // or we can use index
  scores_by_student["박팽년"] = 3.71;
  scores_by_student["김문기"] = 2.72;

  PrintElements(scores_by_student);
  SearchAndPrint(scores_by_student, string("박팽년"));
  SearchAndPrint(scores_by_student, string("김시습"));

  // multimap
  multimap<int, string> m2;
  m2.insert(make_pair(1, "hello"));
  m2.insert(make_pair(1, "annyeong"));
  m2.insert(make_pair(1, "ohayou"));
  m2.insert(make_pair(2, "nihao"));
  m2.insert(make_pair(2, "bonjour"));

  PrintElements(m2);  // print all
  auto range = m2.equal_range(1); // print where key == 1
  for (auto i = range.first; i != range.second; ++i)
    cout << i->first << ": " << i->second << endl;

  return 0;
}