#include <string.h>

#include <iostream>
using namespace std;

/*A null character is a character with all its bits set to zero.
Therefore, it has a numeric value of zero and can be used to represent the end
of a string of characters, such as a word or phrase. This helps programmers
determine the length of strings.
In practical applications, such as database and spreadsheet programs, null
characters are used as fillers for spaces.*/

class PhotonCanon {
 private:
  int hp, shield;
  int x_pos, y_pos;
  int damage;

  char *name;

 public:
  PhotonCanon(int x, int y);
  PhotonCanon(int x, int y, const char *new_name);
  PhotonCanon(const PhotonCanon &cannon);
  ~PhotonCanon();

  void ShowStat();
};

PhotonCanon::PhotonCanon(int x, int y) {
  hp = shield = 100;
  x_pos = x;
  y_pos = y;
  damage = 20;

  name = nullptr;
}

PhotonCanon::PhotonCanon(int x, int y, const char *new_name) {
  hp = shield = 100;
  x_pos = x;
  y_pos = y;
  damage = 20;

  name = new char[strlen(new_name) + 1];  // for C's string has a null
                                          // character.
  strcpy(name, new_name);
}

PhotonCanon::PhotonCanon(const PhotonCanon &cannon) {
  cout << "copy construct" << endl;

  hp = cannon.hp;
  shield = cannon.shield;
  x_pos = cannon.x_pos;
  y_pos = cannon.y_pos;
  damage = cannon.damage;

  name = new char[strlen(cannon.name) + 1];
  strcpy(name, cannon.name);
}

PhotonCanon::~PhotonCanon() {
  cout << "destruct " << name << endl;

  if (name != nullptr) delete[] name;
}

void PhotonCanon::ShowStat() {
  cout << "Photon Cannon, " << name << endl;
  cout << "location: " << x_pos << ", " << y_pos << endl;
  cout << "hp: " << hp << endl;
}

int main() {
  PhotonCanon pc1(3, 3, "Cannon");
  pc1.ShowStat();

  PhotonCanon pc2 = pc1;
  pc2.ShowStat();

  return 0;
}