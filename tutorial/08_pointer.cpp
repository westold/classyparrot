#include <iostream>
using namespace std;

int main() {
  // double pointer
  cout << "Double Pointer" << endl;

  int *ptr1;
  int **ptr2;
  int num = 27;

  ptr1 = &num;
  ptr2 = &ptr1;

  cout << ptr1 << endl;    // address of num
  cout << *ptr1 << endl;   // num
  cout << ptr2 << endl;    // address of ptr1
  cout << *ptr2 << endl;   // address of num
  cout << **ptr2 << endl;  // num

  cout << endl;

  // void pointer (generic pointer)
  cout << "Void Pointer" << endl;

  struct Test {
   public:
    int num;
    string str;
    Test() : num(10), str("hell"){};
  };

  int i = 12;
  string s = "hello, World!";
  Test t;

  void *vPtr;
  vPtr = &i;
  cout << vPtr << endl;  // address of int i

  vPtr = &s;
  cout << vPtr << endl;  // address of string s

  vPtr = &t;
  cout << vPtr << endl;  // address of Test t

  Test *tPtr = (Test *)vPtr;
  cout << tPtr->num << endl;  // num of t

  return 0;
}