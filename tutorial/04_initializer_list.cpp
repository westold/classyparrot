#include <iostream>
using namespace std;

class Marine {
  static int marine_num;

  int hp;
  int x_pos, y_pos;
  const int default_damage;
  bool is_dead;

 public:
  Marine();
  Marine(int x, int y);
  Marine(int x, int y, int default_damage);
  ~Marine();

  int Attack() const;
  Marine &GetAttacked(int damage_get);
  void Move(int x, int y);

  void ShowStat();
  static void ShowMarineNum();
};

int Marine::marine_num = 0;

Marine::Marine()
    : hp(50), x_pos(0), y_pos(0), default_damage(5), is_dead(false) {
  marine_num++;
}

Marine::Marine(int x, int y)
    : hp(50), x_pos(x), y_pos(y), default_damage(5), is_dead(false) {
  marine_num++;
}

Marine::Marine(int x, int y, int default_damage)
    : hp(50),
      x_pos(x),
      y_pos(y),
      default_damage(default_damage),
      is_dead(false) {
  marine_num++;
}

Marine::~Marine() { marine_num--; }

void Marine::Move(int x, int y) {
  x_pos = x;
  y_pos = y;
}

int Marine::Attack() const { return default_damage; }

Marine &Marine::GetAttacked(int damage_get) {
  hp -= damage_get;

  if (hp <= 0) {
    hp = 0;
    is_dead = true;
  }

  return *this;
}

void Marine::ShowStat() {
  cout << "***Marine***" << endl;
  cout << "Location: " << x_pos << ", " << y_pos << endl;
  cout << "HP: " << hp << endl;
  cout << "Total Marine Count: " << marine_num << endl;
  cout << endl;
}

void Marine::ShowMarineNum() {
  cout << "Total Marine Count: " << marine_num << endl;
}

int main() {
  Marine marine1(2, 3, 5);
  marine1.ShowStat();

  Marine marine2(3, 5, 10);
  marine2.ShowStat();

  cout << endl << "marine1 attacks marine2 double time!" << endl;
  marine2.GetAttacked(marine1.Attack()).GetAttacked(marine1.Attack());

  marine1.ShowStat();
  marine2.ShowStat();
}