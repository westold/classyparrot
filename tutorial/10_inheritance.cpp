#include <iostream>
#include <string>
using namespace std;

class Employee {
 public:
  Employee(string name, int age, string position, int rank)
      : name_(name), age_(age), position_(position), rank_(rank) {}

  Employee(const Employee& other) {
    name_ = other.name_;
    age_ = other.age_;
    position_ = other.position_;
    rank_ = other.rank_;
  }

  Employee() {}

  virtual void PrintInfo() {
    cout << name_ << " (" << position_ << " , " << age_ << ") => " << Pay()
         << "만원" << endl;
  }

  virtual int Pay() { return 200 + rank_ * 50; }

  virtual ~Employee(){}

 protected:
  string name_;
  int age_;
  string position_;
  int rank_;
};

class Manager : public Employee {
 public:
  Manager(string name, int age, string position, int rank, int work_year)
      : Employee(name, age, position, rank), work_year_(work_year) {}

  Manager(const Manager& other)
      : Employee(other.name_, other.age_, other.position_, other.rank_) {
    work_year_ = other.work_year_;
  }

  Manager():Employee(){}

  void PrintInfo() override {
    cout << name_ << " (" << position_ << ", " << age_ << ", " << work_year_
         << ") => " << Pay() << "만원" << endl;
  }

  int Pay() override { return 200 + rank_ * 50 + 5 * work_year_; }

 private:
  int work_year_;
};

class EmployeeList {
 public:
  EmployeeList(int alloc_count) : alloc_count_(alloc_count) {
    employees_ = new Employee*[alloc_count_];
    count_ = 0;
  }

  void Add(Employee* employee) {
    if (alloc_count_ == count_) {
      Employee** prevEmployees = employees_;
      alloc_count_ *= 2;
      employees_ = new Employee*[alloc_count_];
      for (int i = 0; i < count_; i++) {
        employees_[i] = prevEmployees[i];
      }
      delete[] prevEmployees;
    }
    employees_[count_] = employee;
    count_++;
  }

  int Count() { return count_; }

  void PrintEmployees() {
    int total_pay = 0;
    for (int i = 0; i < count_; i++) {
      employees_[i]->PrintInfo();
      total_pay += employees_[i]->Pay();
    }
    cout << "total pay: " << total_pay << "만원" << endl;
  }

  ~EmployeeList() {
    for (int i = 0; i < count_; i++) {
      delete employees_[i];
    }
    delete[] employees_;
  }

 private:
  int alloc_count_;
  int count_;
  Employee** employees_;
};



int main() {
  EmployeeList employees(10);
  employees.Add(new Employee("성삼문", 34, "평사원", 1));
  employees.Add(new Employee("하위지", 34, "평사원", 1));
  employees.Add(new Manager("이개", 41, "부장", 7, 12));
  employees.Add(new Manager("유성원", 43, "과장", 4, 15));
  employees.Add(new Manager("박팽년", 43, "차장", 5, 13));
  employees.Add(new Employee("김문기", 36, "대리", 2));
  employees.Add(new Employee("김시습", 36, "인턴", -2));
  employees.PrintEmployees();

  return 0;
}