#include <iostream>
using namespace std;

int main() {
  const char *test = "1234567890abcde";
  int size = 0;

  while (test[size] != '\0') size++;

  cout << test << endl;
  cout << size << endl;

  return 0;
}
