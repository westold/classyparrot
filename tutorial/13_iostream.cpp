#include <iostream>
#include <string>
using namespace std;

int main(){
  //stream buffer
  cout << "stream buffer" << endl;
  string s;
  cin >> s;

  char peek = cin.rdbuf()->snextc();
  if(cin.fail()) cout << "failed" << endl;
  cout << "first char of second word: " << peek << endl;
  cin >> s;
  cout << "read again: " << s << endl;  

  //manipulator
  cout << "hex manipulator" << endl;
  int i;
  while(true){
    //change to 0x
    cin.setf(ios_base::hex, ios_base::basefield);
    cin >> i;
    
    //or you can do this
    // cin >> hex >> i;

    cout << "input :" <<i<<endl;
    if(cin.fail()){
      cout << "input right, please" <<endl;
      cin.clear();
      cin.ignore(100,'\n');
    }
    if(i==0) break;
  }
}

