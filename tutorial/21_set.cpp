#include <functional>
#include <iostream>
#include <set>
#include <string>
#include <unordered_set>
using namespace std;

// set, map are sequence containers
// hash function list
// https://en.cppreference.com/w/cpp/utility/hash

#pragma region Print Functions
template <typename T, typename C>
void PrintElements(set<T, C>& s) {
  for (const T& element : s) {
    cout << element << endl;
  }
}

template <typename T>
void PrintElements(multiset<T>& s) {
  for (const T& element : s) {
    cout << element << endl;
  }
}

template <typename T>
void PrintElements(unordered_set<T>& s) {
  for (const T& element : s) {
    cout << element << endl;
  }
}
#pragma endregion

#pragma region ToDo
class ToDo {
 public:
  ToDo(int priority, string name) : priority_(priority), name_(name) {}

  // if you wanna use operator overriding
  // bool operator<(const ToDo& todo) const {
  //   if (priority_ == todo.priority_) {
  //     return name_ < todo.name_;
  //   }
  //   return priority_ < todo.priority_;
  // }

  bool operator==(const ToDo& todo) const {
    if (priority_ == todo.priority_ && name_ == todo.name_) return true;
    return false;
  }

 private:
  friend struct hash<ToDo>;
  friend class ToDoComparer;
  friend ostream& operator<<(ostream& o, const ToDo& todo) {
    o << "prioriry: " << todo.priority_ << ", " << todo.name_;
    return o;
  }
  int priority_;
  string name_;
};

class ToDoComparer {
 public:
  bool operator()(const ToDo& todo1, const ToDo& todo2) const {
    if (todo1.priority_ == todo2.priority_) return todo1.name_ < todo2.name_;
    return todo1.priority_ < todo2.priority_;
  }
};

namespace std {
template <>
struct hash<ToDo> {
  size_t operator()(const ToDo& todo) const {
    hash<string> hash_function;
    return todo.priority_ ^ (hash_function(todo.name_));
  }
};
}  // namespace std
#pragma endregion

int main() {
  // set
  set<int> s;
  s.insert(1);
  s.insert(5);
  s.insert(2);
  s.insert(4);
  s.insert(3);
  s.insert(4);
  cout << "initial set" << endl;
  PrintElements(s);

  cout << "is 5 in set?: ";
  if (s.find(5) != s.end())
    cout << "yes" << endl;
  else
    cout << "No" << endl;

  cout << "is 12 in set?: ";
  if (s.find(12) != s.end())
    cout << "yes" << endl;
  else
    cout << "No" << endl;
  cout << "------------" << endl;

  // set with custom class
  set<ToDo, ToDoComparer> todo_list;
  todo_list.insert(ToDo(1, "cpp_study"));
  todo_list.insert(ToDo(2, "os_study"));
  todo_list.insert(ToDo(3, "network_study"));
  todo_list.insert(ToDo(2, "game_design"));
  todo_list.insert(ToDo(3, "job_searching"));
  PrintElements(todo_list);
  cout << "------------" << endl;

  cout << "erase custom set" << endl;
  todo_list.erase(todo_list.find(ToDo(2, "os_study")));
  PrintElements(todo_list);

  // multiset
  multiset<string> s2;
  s2.insert("a");
  s2.insert("b");
  s2.insert("a");
  s2.insert("c");
  s2.insert("d");
  s2.insert("c");
  PrintElements(s2);

  // unordered set
  unordered_set<ToDo> todo_list2;
  todo_list2.insert(ToDo(1, "cpp_study"));
  todo_list2.insert(ToDo(2, "os_study"));
  todo_list2.insert(ToDo(3, "network_study"));
  todo_list2.insert(ToDo(2, "game_design"));
  todo_list2.insert(ToDo(3, "job_searching"));
  PrintElements(todo_list2);

  return 0;
}