#include <string>
#include <iostream>

#include "15_excel_util.h"
#include "15_excel_table.h"
#include "15_excel_cell.h"

using std::string;

int main() {
	myexcel::TxtTable table(5, 5);

	table.AddCell(new myexcel::NumberCell(2, &table), 1, 1);
	table.AddCell(new myexcel::NumberCell(3, &table), 1, 2);
	table.AddCell(new myexcel::NumberCell(4, &table), 2, 1);
	table.AddCell(new myexcel::NumberCell(5, &table), 2, 2);
	table.AddCell(new myexcel::StringCell("B2 + B3 * ( C2 + C3 - 2 ) =", &table), 3, 1);
	table.AddCell(new myexcel::ExpCell("B2+B3*(C2+C3-2)", &table), 3, 2);
	std::cout << table.PrintAll() << std::endl;

	// // make html
	// HtmlTable table2(5, 5);
	// ofstream ofs2("./tutorial/15_excel.html");
	// table2.AddCell(new Cell("the Six Loyal", 0, 0, &table), 0, 0);
	// table2.AddCell(new Cell("Sung SahmMoon", 0, 1, &table), 0, 1);
	// table2.AddCell(new Cell("Ha WeJee", 1, 1, &table), 1, 1);
	// ofs2 << table2;
	// ofs2.close();

	// // make csv
	// CsvTable table3(5, 5);
	// ofstream ofs3("./tutorial/15_excel.csv");
	// table3.AddCell(new Cell("the Six Loyal", 0, 0, &table), 0, 0);
	// table3.AddCell(new Cell("Sung SahmMoon", 0, 1, &table), 0, 1);
	// table3.AddCell(new Cell("Ha WeJee", 1, 1, &table), 1, 1);
	// ofs3 << table3;
	// ofs3.close();

	return 0;
}
