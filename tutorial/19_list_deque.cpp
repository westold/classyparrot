#include <deque>
#include <iostream>
#include <list>
using namespace std;

// vector, list, deque are sequence containers

template <typename T>
void PrintElements(list<T>& lst) {
  for (const T& element : lst) {
    cout << element << endl;
  }
}

template <typename T>
void PrintElements(deque<T>& dq) {
  for (const T& element : dq) {
    cout << element << endl;
  }
}

int main() {
  // list
  list<int> ints;
  ints.push_back(1);
  ints.push_back(2);
  ints.push_back(3);
  ints.push_back(4);
  ints.push_back(2);
  cout << "initial list" << endl;
  PrintElements(ints);

  for (list<int>::iterator itr = ints.begin(); itr != ints.end(); ++itr) {
    if (*itr == 2) {
      ints.insert(itr, 5);
    }
  }
  cout << "added list" << endl;
  PrintElements(ints);

  for (list<int>::iterator itr = ints.begin(); itr != ints.end(); ++itr) {
    if (*itr == 5) {
      ints.erase(itr);
      itr = ints.begin();
    }
  }
  cout << "erased list" << endl;
  PrintElements(ints);
  cout << "------------" << endl;

  // deque
  deque<int> dq;
  dq.push_back(1);
  dq.push_back(2);
  dq.push_front(3);
  dq.push_front(4);
  cout << "initial deque" << endl;
  PrintElements(dq);

  dq.pop_front();
  dq.pop_back();
  cout << "erased deque" << endl;
  PrintElements(dq);
}