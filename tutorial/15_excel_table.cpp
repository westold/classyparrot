#include "15_excel_table.h"

namespace myexcel {
	Cell::Cell(Table* table) : table_(table) {}

	Table::Table(int max_row, int max_col) : max_row_(max_row), max_col_(max_col) {
		cells_ = new Cell**[max_row_];
		for (int i = 0; i < max_row_; ++i) {
			cells_[i] = new Cell*[max_col_];
			for (int j = 0; j < max_col_; ++j) {
				cells_[i][j] = nullptr;
			}
		}
	}
	Table::~Table() {
		for (int i = 0; i < max_row_; ++i) {
			for (int j = 0; j < max_col_; ++j) {
				if (cells_[i][j]) delete cells_[i][j];
			}
		}
		for (int i = 0; i < max_row_; ++i) delete[] cells_[i];
		delete[] cells_;
	}
	void Table::AddCell(Cell* cell, int row, int col) {
		if (!(row < max_row_ && col < max_col_)) return;
		if (cells_[row][col]) delete cells_[row][col];
		cells_[row][col] = cell;
	}
	string Table::content(const string& cell_name) {
		int col = cell_name[0] - 'A';
		int row = atoi(cell_name.c_str() + 1) - 1;
		if (row < max_row_ && col < max_col_ && cells_[row][col])
			return cells_[row][col]->content();
		return "";
	}
	string Table::content(int row, int col) {
		if (row < max_row_ && col < max_col_ && cells_[row][col])
			return cells_[row][col]->content();
		return "";
	}
	int Table::content_as_num(const string& cell_name) {
		int col = cell_name[0] - 'A';
		int row = atoi(cell_name.c_str() + 1) - 1;
		if (row < max_row_ && col < max_col_ && cells_[row][col])
			return cells_[row][col]->content_as_num();
		return 0;
	}
	int Table::content_as_num(int row, int col) {
		if (row < max_row_ && col < max_col_ && cells_[row][col])
			return cells_[row][col]->content_as_num();
		return 0;
	}

	TxtTable::TxtTable(int row, int col) : Table(row, col) {}
	string TxtTable::PrintAll() {
		string table_txt;
		// set column widths
		int* col_widths = new int[max_col_];
		for (int i = 0; i < max_col_; ++i) {
			int cur_col_width = 2;
			for (int j = 0; j < max_row_; ++j) {
				if (cells_[j][i] && cells_[j][i]->content().length() > cur_col_width) {
					cur_col_width = cells_[j][i]->content().length();
				}
			}
			col_widths[i] = cur_col_width;
		}
		// draw column
		table_txt += "    ";
		int table_width = 4;
		for (int i = 0; i < max_col_; ++i) {
			if (col_widths[i]) {
				int cell_width = std::max(2, col_widths[i]);
				table_txt += " | " + ColumnNumToString(i);
				table_txt += RepeatChar(cell_width - ColumnNumToString(i).length(), ' ');
				table_width += (cell_width + 3);
			}
		}
		// draw row
		table_txt += "\n";
		for (int i = 0; i < max_row_; ++i) {
			table_txt += RepeatChar(table_width, '-');
			table_txt += "\n" + std::to_string(i + 1);
			table_txt += RepeatChar(4 - std::to_string(i + 1).length(), ' ');
			for (int j = 0; j < max_col_; ++j) {
				if (col_widths[j]) {
					int cell_width = std::max(2, col_widths[j]);
					string content = "";
					if (cells_[i][j]) {
						content = cells_[i][j]->content();
					}
					table_txt += " | " + content;
					table_txt += RepeatChar(cell_width - content.length(), ' ');
				}
			}
			table_txt += "\n";
		}
		return table_txt;
	}
	string TxtTable::RepeatChar(int count, char c) {
		string repeated = "";
		for (int i = 0; i < count; ++i) repeated.push_back(c);
		return repeated;
	}
	string TxtTable::ColumnNumToString(int col_num) {
		string col_num_txt = "";
		if (col_num < 26) {
			col_num_txt += ('A' + col_num);
		}
		else {
			char first = 'A' + col_num / 26 - 1;
			char second = 'A' + col_num % 26;
			col_num_txt += first;
			col_num_txt += second;
		}
		return col_num_txt;
	}

	HtmlTable::HtmlTable(int row, int col) : Table(row, col) {}
	string HtmlTable::PrintAll() {
		string table_txt = "<table border='1' cellpadding='10'>";
		for (int i = 0; i < max_row_; ++i) {
			table_txt += "<tr>";
			for (int j = 0; j < max_col_; ++j) {
				table_txt += "<td>";
				if (cells_[i][j]) table_txt += cells_[i][j]->content();
				table_txt += "</td>";
			}
			table_txt += "</tr>";
		}
		table_txt += "</table>";
		return table_txt;
	}

	CsvTable::CsvTable(int row, int col) : Table(row, col) {}
	string CsvTable::PrintAll() {
		string table_txt = "";
		for (int i = 0; i < max_row_; ++i) {
			for (int j = 0; j < max_col_; ++j) {
				if (j >= 1) table_txt += ",";
				string content;
				if (cells_[i][j]) content = cells_[i][j]->content();
				for (int k = 0; k < content.length(); k++) {
					if (content[k] == '"') {
						content.insert(k, 1, '"');
						++k;
					}
				}
				content = '"' + content + '"';
				table_txt += content;
			}
			table_txt += "\n";
		}
		return table_txt;
	}
}