#include <iostream>
using namespace std;

class A {
  int x;

 public:
  A(int c) : x(c) {}

  int &AccessX() { return x; }
  int GetX() { return x; }
  void ShowX() { cout << x << endl; }
};

int main() {
  A a(5);
  a.ShowX();  // return 5

  int &c = a.AccessX();
  c = 4;
  a.ShowX();  // return 4, call by reference.

  int d = a.AccessX();
  d = 3;
  a.ShowX();  // return 4, for d is not a reference. call by value.

  // error
  // int& e = a.GetX(); //can't get temp object's reference.
  // e = 2;
  // a.ShowX();

  int f = a.GetX();  // call by value.
  f = 1;
  a.ShowX();  // return 4
}