#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

#pragma region R / W Fuctions
void ReadAll(string file_path) {
  ifstream ifs(file_path);
  string str;

  if (!ifs.is_open()) {
    cout << "couldn't find the file!" << endl;
    return;
  }

  ifs.seekg(0, ios::end);
  int size = ifs.tellg();
  str.resize(size);
  ifs.seekg(0, ios::beg);
  ifs.read(&str[0], size);
  cout << str << endl;
}

void ReadByLine(string file_path) {
  ifstream ifs(file_path);
  string buffer;

  if (!ifs.is_open()) {
    cout << "couldn't find the file!" << endl;
    return;
  }

  while (getline(ifs, buffer)) {
    cout << buffer << endl;
  }
}

void Write(string file_path, string add_str) {
  ofstream ofs(file_path, ios::app);

  if (ofs.is_open()) {
    ofs << add_str;
  }
}
#pragma endregion
#pragma region OpertaorOverriding
class Human {
 public:
  Human(const string& name, int age) : name_(name), age_(age) {}
  string GetInfo() { return "Name: " + name_ + " / Age: " + to_string(age_); }

 private:
  string name_;
  int age_;
  friend ofstream& operator<<(ofstream& ofs, Human& human);
};

ofstream& operator<<(ofstream& ofs, Human& human) {
  ofs << human.GetInfo();
  return ofs;
}
#pragma endregion
#pragma region StringStream
int ToNumber(string str) {
  std::istringstream iss(str);
  int x;
  iss >> x;
  return x;
}
string ToString(int x){
  std::ostringstream oss;
  oss << x;
  return oss.str();
}
#pragma endregion
int main() {
  // Read All
  string file_path = "./tutorial/14_file_stream.txt";
  ReadAll(file_path);

  // Write
  Write(file_path, "Bomb! pretty atom~\n");
  ReadByLine(file_path);

  // Operator Overriding
  ofstream ofs(file_path, ios::app);
  Human fiveStars("Five Stars", 57);
  ofs << fiveStars << endl;
  ofs.close();
  ReadAll(file_path);

  // String Stream
  cout << "strToInt: 19 + 57 = " << ToNumber("19") + ToNumber("57") << endl;
  cout << "intToStr: 19 + 57 = " << ToString(19+57) << endl;
  return 0;
}