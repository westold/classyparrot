#pragma once
#include <string>
#include <time.h>
#include "15_excel_util.h"
#include "15_excel_table.h"

using std::string;

namespace myexcel {
	class StringCell : public Cell {
	public:
		StringCell(string content, Table* t);
		string content();
		int content_as_num();

	private:
		string content_;
	};

	class NumberCell : public Cell {
	public:
		NumberCell(int content, Table* table);
		string content();
		int content_as_num();

	private:
		int content_;
	};

	class DateCell : public Cell {
	public:
		DateCell(string content, Table* table);
		string content();
		int content_as_num();

	private:
		time_t content_;
	};

	class ExpCell : public Cell {
	public:
		ExpCell(string content, Table* table);
		string content();
		int content_as_num();

	private:
		int GetPriority(char c);
		void ParseExpression();
		string content_;
		Vector exp_vec_;
	};
}