#include <iostream>

using namespace std;

class MyString {
 private:
  char *content;
  int length;
  int memoryCapacity;
  int GetSize(const char *str);

 public:
  explicit MyString(int capacity);
  MyString(char c);
  MyString(const char *str);
  MyString(const MyString &str);

  int Length() const;
  int Capacity() const;
  void Reserve(int size);

  MyString &Assgin(const char *str);
  MyString &Assgin(const MyString &str);

  MyString &Insert(int index, char c);
  MyString &Insert(int index, const char *str);
  MyString &Insert(int index, const MyString &str);

  MyString &Erase(int index, int count);

  int Find(int from, char c) const;
  int Find(int from, const char *str) const;
  int Find(int from, const MyString &str) const;

  int Compare(const MyString &str) const;

  void Print() const;

  bool operator==(const MyString &str) const;
  MyString &operator+(const MyString &str);
  char &operator[](int index) const;

  ~MyString();
};

int MyString::GetSize(const char *str) {
  int size = 0;

  while (str[size] != '\0') size++;

  return size;
}

MyString::MyString(int capacity) {
  content = new char[capacity];
  length = 0;
  memoryCapacity = capacity;
}

MyString::MyString(char c) {
  content = new char[1];
  content[0] = c;
  length = 1;
  memoryCapacity = 1;
}

MyString::MyString(const char *str) {
  length = GetSize(str);
  memoryCapacity = length;
  content = new char[length];

  for (int i = 0; i < length; i++) content[i] = str[i];
}

MyString::MyString(const MyString &str) {
  length = str.length;
  memoryCapacity = length;
  content = new char[length];

  for (int i = 0; i < length; i++) content[i] = str.content[i];
}

int MyString::Length() const { return length; }

int MyString::Capacity() const { return memoryCapacity; }

void MyString::Reserve(int size) {
  if (size > memoryCapacity) {
    char *previousContent = content;

    content = new char[size];
    memoryCapacity = size;

    for (int i = 0; i < length; i++) content[i] = previousContent[i];

    delete[] previousContent;
  }
}

MyString &MyString::Assgin(const char *str) {
  int newLength = GetSize(str);

  if (newLength > memoryCapacity) {
    delete[] content;
    content = new char[newLength];
    memoryCapacity = newLength;
  }

  for (int i = 0; i < newLength; i++) content[i] = str[i];

  length = newLength;

  return *this;
}

MyString &MyString::Assgin(const MyString &str) {
  if (str.length > memoryCapacity) {
    delete[] content;
    content = new char[str.length];
    memoryCapacity = str.length;
  }

  for (int i = 0; i < str.length; i++) content[i] = str.content[i];

  length = str.length;

  return *this;
}

MyString &MyString::Insert(int index, char c) {
  MyString temp(c);
  return Insert(index, temp);
}

MyString &MyString::Insert(int index, const char *str) {
  MyString temp(str);
  return Insert(index, temp);
}

MyString &MyString::Insert(int index, const MyString &str) {
  if (index > length || index < 0) return *this;

  if (length + str.length > memoryCapacity) {
    if (memoryCapacity * 2 > length + str.length)
      memoryCapacity *= 2;
    else
      memoryCapacity = length + str.length;

    char *previousContent = content;
    content = new char[memoryCapacity];

    for (int i = 0; i < index; i++) content[i] = previousContent[i];

    for (int i = 0; i < str.length; i++) content[index + i] = str.content[i];

    for (int i = index; i < length; i++)
      content[str.length + i] = previousContent[i];

    delete[] previousContent;
    length = length + str.length;

    return *this;
  }

  for (int i = length - 1; i >= index; i--)
    content[i + str.length] = content[i];

  for (int i = 0; i < str.length; i++) content[i + index] = str.content[i];

  length = length + str.length;

  return *this;
}

MyString &MyString::Erase(int index, int count) {
  if (count < 0 || index < 0 || index > length) return *this;

  if (index + count > length) count = length - index;

  for (int i = index + count; i < length; i++) {
    content[i - count] = content[i];
  }

  length -= count;
  return *this;
}

int MyString::Find(int from, char c) const {
  MyString temp(c);
  return Find(from, temp);
}

int MyString::Find(int from, const char *str) const {
  MyString temp(str);
  return Find(from, temp);
}

// boyer-moore algorithm
int MyString::Find(int from, const MyString &str) const {
  int result = -1;

  int tLen = length;
  int pLen = str.length;

  int pivot = pLen - 1 + from;

  while (pivot < tLen) {
    int i = pivot;
    int j = pLen - 1;

    // find mismatch
    while (content[i] == str.content[j]) {
      i--;
      j--;

      if (j == -1) {
        result = i + 1;
        return result;
      }
    }

    // handle bad character
    int k;

    for (k = j - 1; k >= 0; k--) {
      if (content[i] == str.content[k]) break;
    }

    if (k < 0)
      pivot += pLen;
    else
      pivot += j - k;
  }

  return result;
}

int MyString::Compare(const MyString &str) const {
  int shortLength = length < str.length ? length : str.length;

  for (int i = 0; i < shortLength; i++) {
    if (content[i] > str.content[i])
      return 1;

    else if (content[i] < str.content[i])
      return -1;
  }

  if (length == str.length)
    return 0;

  else if (length > str.length)
    return 1;

  return -1;
}

void MyString::Print() const {
  for (int i = 0; i < length; i++) cout << content[i];

  cout << endl;
}

bool MyString::operator==(const MyString &str) const { return !Compare(str); }

MyString &MyString::operator+(const MyString &str) {
  return Insert(length, str);
}

char &MyString::operator[](int index) const { return content[index]; }

MyString::~MyString() { delete[] content; }

int main() {
  // erase
  MyString str0("Hell World?");
  str0.Print();
  str0.Erase(4, 3);

  cout << "Erase 3 char from index 4: ";
  str0.Print();

  // find
  MyString str1("Hell or World? Again, hell or world?!");
  MyString str2("World");

  cout << "Found index: " << str1.Find(0, str2) << endl;

  // compare
  MyString str3("hello!");
  MyString str4("hell");

  cout << "Compare: " << str3.Compare(str4) << endl;

  // add
  MyString str5("hello!");
  MyString str6("hell");

  str5 = str5 + str6;
  str5.Print();

  // indexing
  cout << "2nd char: " << str5[1] << endl;

  return 0;
}
