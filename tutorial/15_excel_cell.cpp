#include "15_excel_cell.h"

namespace myexcel {
	StringCell::StringCell(string content, Table* table)
		: content_(content), Cell(table) {}
	string StringCell::content() { return content_; };
	int StringCell::content_as_num() { return 0; }

	NumberCell::NumberCell(int content, Table* table)
		: content_(content), Cell(table) {}
	string NumberCell::content() { return std::to_string(content_); }
	int NumberCell::content_as_num() { return content_; }

	// Date format is yyyy-mm-dd.
	// C++ time_t struct, https://calvinjmkim.tistory.com/31
	DateCell::DateCell(string content, Table* table)
		: Cell(table) {
		int year = atoi(content.c_str());
		int month = atoi(content.c_str() + 5);
		int day = atoi(content.c_str() + 8);
		tm time_info;
		time_info.tm_year = year - 1900;
		time_info.tm_mon = month - 1;
		time_info.tm_mday = day;
		time_info.tm_hour = 0;
		time_info.tm_min = 0;
		time_info.tm_sec = 0;
		content_ = mktime(&time_info);
	}
	string DateCell::content() {
		char buf[50];
		tm temp;
		localtime_s(&temp, &content_);
		strftime(buf, 50, "%F", &temp);
		return string(buf);
	}
	int DateCell::content_as_num() { return static_cast<int>(content_); }

	ExpCell::ExpCell(string content, Table* table)
		: content_(content), Cell(table) {
		ParseExpression();
	}
	string ExpCell::content() { return std::to_string(content_as_num()); }
	int ExpCell::content_as_num() {
		double result = 0;
		NumStack stack;
		for (int i = 0; i < exp_vec_.length(); ++i) {
			string cur_exp = exp_vec_[i];
			if (isalpha(cur_exp[0])) {
				int temp = table_->content_as_num(cur_exp);
				stack.Push(temp);
				continue;
			}
			if (isdigit(cur_exp[0])) {
				stack.Push(atoi(cur_exp.c_str()));
				continue;
			}
			double b = stack.Pop();
			double a = stack.Pop();
			switch (cur_exp[0]) {
			case '+':
				stack.Push(a + b);
				break;
			case '-':
				stack.Push(a - b);
				break;
			case '*':
				stack.Push(a * b);
				break;
			case '/':
				stack.Push(a / b);
				break;
			}
		}
		return stack.Pop();
	}
	int ExpCell::GetPriority(char c) {
		switch (c) {
		case '(':
		case '{':
		case '[':
			return 0;
		case '+':
		case '-':
			return 1;
		case '*':
		case '/':
			return 2;
		}
		return 0;
	}
	void ExpCell::ParseExpression() {
		Stack stack;
		content_.insert(0, "(");
		content_.push_back(')');
		for (int i = 0; i < content_.length(); ++i) {
			if (isalpha(content_[i])) {
				exp_vec_.PushBack(content_.substr(i, 2));
				++i;
			}
			else if (isdigit(content_[i])) {
				exp_vec_.PushBack(content_.substr(i, 1));
			}
			else if (content_[i] == '(' || content_[i] == '{' || content_[i] == '[') {
				stack.Push(content_.substr(i, 1));
			}
			else if (content_[i] == ')' || content_[i] == '}' || content_[i] == ']') {
				string element = stack.Pop();
				while (element != "(" && element != "{" && element != "[") {
					exp_vec_.PushBack(element);
					element = stack.Pop();
				}
			}
			else if (content_[i] == '+' || content_[i] == '-' || content_[i] == '*' ||
				content_[i] == '/') {
				while (!stack.IsEmpty() &&
					GetPriority(stack.Peek()[0]) >= GetPriority(content_[i])) {
					exp_vec_.PushBack(stack.Pop());
				}
				stack.Push(content_.substr(i,1));
			}
		}
	}
}