#include <iostream>

using namespace std;

class MyString {
 private:
  char *content_;
  int length_;
  int memory_capacity_;
  int GetSize(const char *str);

 public:
  explicit MyString(int capacity);
  MyString(char c);
  MyString(const char *str);
  MyString(const MyString &str);

  int Length() const;
  int Capacity() const;
  void Reserve(int size);

  MyString &Assgin(const char *str);
  MyString &Assgin(const MyString &str);

  MyString &Insert(int index, char c);
  MyString &Insert(int index, const char *str);
  MyString &Insert(int index, const MyString &str);

  MyString &Erase(int index, int count);

  int Find(int from, char c) const;
  int Find(int from, const char *str) const;
  int Find(int from, const MyString &str) const;

  int Compare(const MyString &str) const;

  void Print() const;

  bool operator==(const MyString &str) const;
  MyString &operator+(const MyString &str);
  char &operator[](int index) const;

  ~MyString();
};

int MyString::GetSize(const char *str) {
  int size = 0;

  while (str[size] != '\0') size++;

  return size;
}

MyString::MyString(int capacity) {
  content_ = new char[capacity];
  length_ = 0;
  memory_capacity_ = capacity;
}

MyString::MyString(char c) {
  content_ = new char[1];
  content_[0] = c;
  length_ = 1;
  memory_capacity_ = 1;
}

MyString::MyString(const char *str) {
  length_ = GetSize(str);
  memory_capacity_ = length_;
  content_ = new char[length_];

  for (int i = 0; i < length_; i++) content_[i] = str[i];
}

MyString::MyString(const MyString &str) {
  length_ = str.length_;
  memory_capacity_ = length_;
  content_ = new char[length_];

  for (int i = 0; i < length_; i++) content_[i] = str.content_[i];
}

int MyString::Length() const { return length_; }

int MyString::Capacity() const { return memory_capacity_; }

void MyString::Reserve(int size) {
  if (size > memory_capacity_) {
    char *previousContent = content_;

    content_ = new char[size];
    memory_capacity_ = size;

    for (int i = 0; i < length_; i++) content_[i] = previousContent[i];

    delete[] previousContent;
  }
}

MyString &MyString::Assgin(const char *str) {
  int new_length = GetSize(str);

  if (new_length > memory_capacity_) {
    delete[] content_;
    content_ = new char[new_length];
    memory_capacity_ = new_length;
  }

  for (int i = 0; i < new_length; i++) content_[i] = str[i];

  length_ = new_length;

  return *this;
}

MyString &MyString::Assgin(const MyString &str) {
  if (str.length_ > memory_capacity_) {
    delete[] content_;
    content_ = new char[str.length_];
    memory_capacity_ = str.length_;
  }

  for (int i = 0; i < str.length_; i++) content_[i] = str.content_[i];

  length_ = str.length_;

  return *this;
}

MyString &MyString::Insert(int index, char c) {
  MyString temp(c);
  return Insert(index, temp);
}

MyString &MyString::Insert(int index, const char *str) {
  MyString temp(str);
  return Insert(index, temp);
}

MyString &MyString::Insert(int index, const MyString &str) {
  if (index > length_ || index < 0) return *this;

  if (length_ + str.length_ > memory_capacity_) {
    if (memory_capacity_ * 2 > length_ + str.length_)
      memory_capacity_ *= 2;
    else
      memory_capacity_ = length_ + str.length_;

    char *previousContent = content_;
    content_ = new char[memory_capacity_];

    for (int i = 0; i < index; i++) content_[i] = previousContent[i];

    for (int i = 0; i < str.length_; i++) content_[index + i] = str.content_[i];

    for (int i = index; i < length_; i++)
      content_[str.length_ + i] = previousContent[i];

    delete[] previousContent;
    length_ = length_ + str.length_;

    return *this;
  }

  for (int i = length_ - 1; i >= index; i--)
    content_[i + str.length_] = content_[i];

  for (int i = 0; i < str.length_; i++) content_[i + index] = str.content_[i];

  length_ = length_ + str.length_;

  return *this;
}

MyString &MyString::Erase(int index, int count) {
  if (count < 0 || index < 0 || index > length_) return *this;

  if (index + count > length_) count = length_ - index;

  for (int i = index + count; i < length_; i++) {
    content_[i - count] = content_[i];
  }

  length_ -= count;
  return *this;
}

int MyString::Find(int from, char c) const {
  MyString temp(c);
  return Find(from, temp);
}

int MyString::Find(int from, const char *str) const {
  MyString temp(str);
  return Find(from, temp);
}

// boyer-moore algorithm
int MyString::Find(int from, const MyString &str) const {
  int result = -1;

  int tLen = length_;
  int pLen = str.length_;

  int pivot = pLen - 1 + from;

  while (pivot < tLen) {
    int i = pivot;
    int j = pLen - 1;

    // find mismatch
    while (content_[i] == str.content_[j]) {
      i--;
      j--;

      if (j == -1) {
        result = i + 1;
        return result;
      }
    }

    // handle bad character
    int k;

    for (k = j - 1; k >= 0; k--) {
      if (content_[i] == str.content_[k]) break;
    }

    if (k < 0)
      pivot += pLen;
    else
      pivot += j - k;
  }

  return result;
}

int MyString::Compare(const MyString &str) const {
  int shortLength = length_ < str.length_ ? length_ : str.length_;

  for (int i = 0; i < shortLength; i++) {
    if (content_[i] > str.content_[i])
      return 1;

    else if (content_[i] < str.content_[i])
      return -1;
  }

  if (length_ == str.length_)
    return 0;

  else if (length_ > str.length_)
    return 1;

  return -1;
}

void MyString::Print() const {
  for (int i = 0; i < length_; i++) cout << content_[i];

  cout << endl;
}

bool MyString::operator==(const MyString &str) const { return !Compare(str); }

MyString &MyString::operator+(const MyString &str) {
  return Insert(length_, str);
}

char &MyString::operator[](int index) const { return content_[index]; }

MyString::~MyString() { delete[] content_; }

int main() {
  // erase
  MyString str0("Hell World?");
  str0.Print();
  str0.Erase(4, 3);

  cout << "Erase 3 char from index 4: ";
  str0.Print();

  // find
  MyString str1("Hell or World? Again, hell or world?!");
  MyString str2("World");

  cout << "Found index: " << str1.Find(0, str2) << endl;

  // compare
  MyString str3("hello!");
  MyString str4("hell");

  cout << "Compare: " << str3.Compare(str4) << endl;

  // add
  MyString str5("hello!");
  MyString str6("hell");

  str5 = str5 + str6;
  str5.Print();

  // indexing
  cout << "2nd char: " << str5[1] << endl;

  return 0;
}