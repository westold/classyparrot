#include "15_excel_util.h"
namespace myexcel {
#pragma region Vector
Vector::Vector(int n) : data_(new string[n]), capacity_(n), length_(0){};
Vector::~Vector() {
  if (data_) delete[] data_;
};

void Vector::PushBack(string s) {
  if (capacity_ <= length_) {
    string* temp = new string[capacity_ * 2];
    for (int i = 0; i < length_; ++i) {
      temp[i] = data_[i];
    }
    delete[] data_;
    data_ = temp;
    capacity_ *= 2;
  }
  data_[length_] = s;
  ++length_;
};
void Vector::RemoveAt(int index) {
  if (index < 0 || index >= length_) return;
  for (int i = index; i < length_ - 1; ++i) {
    data_[i] = data_[i + 1];
  }
  --length_;
};
string Vector::operator[](int i) { return data_[i]; };
int Vector::length() { return length_; };
#pragma endregion

#pragma region Stack
Stack::Stack() : start_(nullptr, "") { current_ = &start_; };
Stack::~Stack() {
  while (current_ != &start_) {
    Node* to_delete = current_;
    current_ = current_->pre_;
    delete to_delete;
  }
};
void Stack::Push(string str) {
  Node* new_node = new Node(current_, str);
  current_ = new_node;
};
string Stack::Pop() {
  if (current_ == &start_) return "";
  string to_pop = current_->content_;
  Node* to_delete = current_;
  current_ = current_->pre_;
  delete to_delete;
  return to_pop;
};
string Stack::Peek() { return current_->content_; };
bool Stack::IsEmpty() {
  if (current_ == &start_) return true;
  return false;
};
#pragma endregion

#pragma region NumStack
NumStack::NumStack() : start_(nullptr, 0) { current_ = &start_; };
NumStack::~NumStack() {
  while (current_ != &start_) {
    Node* to_delete = current_;
    current_ = current_->pre_;
    delete to_delete;
  }
};

void NumStack::Push(double num) {
  Node* new_node = new Node(current_, num);
  current_ = new_node;
};
double NumStack::Pop() {
  if (current_ == &start_) return 0;
  double to_pop = current_->content_;
  Node* to_delete = current_;
  current_ = current_->pre_;
  delete to_delete;
  return to_pop;
};
double NumStack::Peek() { return current_->content_; };
bool NumStack::IsEmpty() {
  if (current_ == &start_) return true;
  return false;
};
#pragma endregion
}  // namespace myexcel