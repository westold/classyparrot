#include <string.h>

#include <iostream>
using namespace std;

// Knuth-Morris-Pratt
int KMP(const string &target, const string &pattern) { return -1; }

// Boyer-Moore, BadCharcterRule
int BoyerMoore(const string &target, const string &pattern) {
  int result = -1;

  int tLen = target.size();
  int pLen = pattern.size();

  int pivot = pLen - 1;

  while (pivot < tLen) {
    int i = pivot;
    int j = pLen - 1;

    // find mismatch
    while (target[i] == pattern[j]) {
      i--;
      j--;

      if (j == -1) {
        result = i + 1;
        return result;
      }
    }

    // handle bad character
    int k;

    for (k = j - 1; k >= 0; k--) {
      if (target[i] == pattern[k]) break;
    }

    if (k < 0)
      pivot += pLen;
    else
      pivot += j - k;
  }

  return result;
}

int main() {
  string target = "gcaatgcctatgtgacc";
  string pattern = "tatgtg";

  cout << BoyerMoore(target, pattern) << endl;

  return 0;
}