#include <iostream>
using namespace std;

//What are some disciplines for using multiple inheritance?
//https://isocpp.org/wiki/faq/multiple-inheritance#mi-not-evil

class A {
 public:
  A() { cout << "creating A" << endl; }
  virtual ~A() { cout << "deleting A" << endl; }

  int a;
};

class B {
 public:
  B() { cout << "creating B" << endl; }
  virtual ~B() { cout << "deleting B" << endl; }

  int b;
};

class C : public B, public A {
 public:
  C() { cout << "creating C" << endl; }
  virtual ~C() { cout << "deleting C" << endl; }

  int c;
};

int main(){
  A* test = new C();
  delete test;
}
