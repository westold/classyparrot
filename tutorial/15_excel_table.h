#pragma once
#include <string>
#include <algorithm>
#include <iostream>
#include "15_excel_util.h"

using namespace std;

namespace myexcel {
	class Table;

	class Cell {
	public:
		Cell(Table* table);
		virtual string content() = 0;
		virtual int content_as_num() = 0;

	protected:
		Table* table_;
	};

	class Table {
	public:
		Table(int max_row, int max_col);
		~Table();

		void AddCell(Cell* cell, int row, int col);
		virtual string PrintAll() = 0;
		string content(const string& cell_name);
		string content(int row, int col);
		int content_as_num(const string& cell_name);
		int content_as_num(int row, int col);

	protected:
		int max_row_, max_col_;
		Cell*** cells_;
	};

	class TxtTable : public Table {
	public:
		TxtTable(int row, int col);
		string PrintAll();

	private:
		string RepeatChar(int count, char c);
		string ColumnNumToString(int col_num);
	};

	// table, tr, th, td tag. https://aboooks.tistory.com/59
	class HtmlTable : public Table {
	public:
		HtmlTable(int row, int col);
		string PrintAll();
	};

	// CSV wikipeida, https://en.wikipedia.org/wiki/Comma-separated_values
	class CsvTable : public Table {
	public:
		CsvTable(int row, int col);
		string PrintAll();
	};
}