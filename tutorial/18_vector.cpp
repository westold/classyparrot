#include <iostream>
#include <vector>
using namespace std;

// vector, list, deque are sequence containers

// https://stackoverflow.com/questions/51818053/correct-usage-of-vectorintsize-type
// https://stackoverflow.com/questions/610245/where-and-why-do-i-have-to-put-the-template-and-typename-keywords

template <typename T>
void PrintVector(vector<T>& vec) {
  for (typename vector<T>::iterator i = vec.begin(); i != vec.end(); ++i) {
    cout << *i << endl;
  }
}

template <typename T>
void PrintVectorRangeBased(vector<T>& vec){
  for (const T& element : vec){
    cout << element << endl;
  }
}

int main() {
  // container
  vector<int> vec1;
  vec1.push_back(1);
  vec1.push_back(2);
  vec1.push_back(3);
  vec1.push_back(4);

  for (int i = 0; i < vec1.size(); ++i) {  // why should I use size_type here?
    cout << i << " element: " << vec1[i] << endl;
  }

  // iterator
  vector<int> vec2;
  vec2.push_back(5);
  vec2.push_back(6);
  vec2.push_back(7);
  vec2.push_back(8);

  for (vector<int>::iterator i = vec2.begin(); i != vec2.end(); ++i) {
    cout << *i << endl;
  }

  vector<int>::iterator i = vec2.begin() + 2;
  cout << "3rd element: " << *i << endl;

  // insert & erase
  vector<int> vec3;
  vec3.push_back(9);
  vec3.push_back(10);
  vec3.push_back(14);
  vec3.push_back(12);

  cout << "initial vector" << endl;
  PrintVector(vec3);

  cout << "insert" << endl;
  vec3.insert(vec3.begin() + 2, 11);
  PrintVector(vec3);

  cout << "erase" << endl;
  vec3.erase(vec3.begin() + 3);
  PrintVector(vec3);

  cout << "------------" << endl;

  // iterative erase // lator, we're gonna change it with <algorithm>
  vector<int> vec4;
  vec4.push_back(13);
  vec4.push_back(14);
  vec4.push_back(15);
  vec4.push_back(16);
  vec4.push_back(15);

  cout << "initial vector" << endl;
  PrintVector(vec4);

  
  for (vector<int>::iterator itr = vec4.begin(); itr != vec4.end(); ++itr) {
    if (*itr == 15) {
      vec4.erase(itr);
      itr = vec4.begin();
    }
  }

  // or we can just do like it
  // for (int i = 0; i != vec4.size(); ++i) {
  //   if (vec4[i] == 15) {
  //     vec4.erase(vec4.begin() + i);
  //     --i;
  //   }
  // }

  cout << "erased vector" << endl;
  PrintVector(vec4);
  cout << "------------" << endl;

  // const_iterator
  // can't change a value the const iterator points
  // get with cbegin(), cend()

  // reverse_iterator
  cout << "reverse iterator" << endl;
  for (vector<int>::reverse_iterator ritr = vec4.rbegin(); ritr != vec4.rend();
       ++ritr) {
    cout << *ritr << endl;
  }

  // or can just do like it
  // for (int i = vec4.size() - 1; i >= 0; i--) {
  //   cout << vec4[i] << endl;
  // }

  cout << "------------" << endl;

  // you can also use const_reverse_iterator
  // get with crbegin(), crend()

  //range based for-loop
  vector<int> vec5;
  vec5.push_back(17);
  vec5.push_back(18);
  vec5.push_back(19);
  vec5.push_back(20);

  cout << "ranged based print" << endl;
  PrintVectorRangeBased(vec5);
}