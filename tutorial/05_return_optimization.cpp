#include <iostream>
using namespace std;

/*In a return statement, when the operand is the name of a non-volatile object
with automatic storage duration, which isn't a function parameter or a catch
clause parameter, and which is of the same class type (ignoring
cv-qualification) as the function return type. This variant of copy elision is
known as NRVO, "named return value optimization".*/

class A {
  int x;

 public:
  A(int c) : x(c) {}
  A(const A &a) {
    x = a.x;
    cout << "copy" << endl;
  }
};

class B {
  A a;

 public:
  B(int c) : a(c) {}
  B(const B &b) : a(b.a) {}
  A GetA() {
    A temp(a);
    return temp;
  }
};

int main() {
  B b(10);

  std::cout << "---------" << std::endl;
  A a1 = b.GetA();  // string "copy" is returned only once, because of NRVO.
}