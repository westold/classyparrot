#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

template <typename Itr>
void Print(Itr begin, Itr end) {
  while (begin != end) {
    cout << *begin << " ";
    ++begin;
  }
  cout << endl;
}

template <typename T>
class LargerComparer {
 public:
  bool operator()(const T& a, const T& b) const { return a > b; }
};

class User {
 public:
  string name_;
  int age_;
  User(string name, int age) : name_(name), age_(age) {}
  bool operator<(const User& user) const { return age_ < user.age_; }
  bool operator==(const User& user) const {
    if (name_ == user.name_ && age_ == user.age_) return true;
    return false;
  }

 private:
  friend ostream& operator<<(ostream& o, const User& user) {
    o << "[" << user.age_ << ", " << user.name_ << "]";
    return o;
  }
};

class NationalPark {
 public:
  bool AddUser(string name, int age) {
    User new_user(name, age);
    if (find(users.begin(), users.end(), new_user) != users.end()) return false;
    users.push_back(new_user);
    return true;
  }
  bool CanGetOldGroupDiscount() {
    return all_of(users.begin(), users.end(),
                  [](User& user) { return user.age_ >= 65; });
  }
  bool CanGetChildDiscount() {
    return any_of(users.begin(), users.end(), [](User& user){
      return user.age_ <= 7;});
  }

 private:
  vector<User> users;
};

int main() {
  vector<int> vec;
  vec.push_back(3);
  vec.push_back(5);
  vec.push_back(1);
  vec.push_back(8);
  vec.push_back(2);
  vec.push_back(4);
  vec.push_back(6);
  vec.push_back(7);

  // sort
  cout << "before sort" << endl;
  Print(vec.begin(), vec.end());

  sort(vec.begin(), vec.end());
  cout << "after sort" << endl;
  Print(vec.begin(), vec.end());

  // sort with comparer
  // sort(vec.begin(), vec.end(), LargerComparer<int>());
  // cout << "reverse sort" << endl;
  // Print(vec.begin(), vec.end());

  // or we can just do this
  sort(vec.begin(), vec.end(), greater<int>());
  cout << "reverse sort" << endl;
  Print(vec.begin(), vec.end());

  // partial sort
  partial_sort(vec.begin(), vec.begin() + 3, vec.end());
  cout << "partial sort" << endl;
  Print(vec.begin(), vec.end());

  // stable sort
  vector<User> users;
  for (int i = 0; i < 100; ++i) {
    string name = "";
    name.push_back('a' + i / 26);
    name.push_back('a' + i % 26);
    users.push_back(User(name, static_cast<int>(rand() % 50)));
  }

  vector<User> users2 = users;
  cout << "before sort" << endl;
  Print(users.begin(), users.end());

  sort(users.begin(), users.end());
  cout << "after sort" << endl;
  Print(users.begin(), users.end());

  stable_sort(users2.begin(), users2.end());
  cout << "stable sort" << endl;
  Print(users2.begin(), users2.end());
  cout << "------------" << endl;

  // remove
  vector<int> vec2;
  vec2.push_back(3);
  vec2.push_back(5);
  vec2.push_back(1);
  vec2.push_back(8);
  vec2.push_back(2);
  vec2.push_back(4);
  vec2.push_back(6);
  vec2.push_back(7);

  cout << "initial vector" << endl;
  Print(vec2.begin(), vec2.end());

  cout << "remove 2 odd number" << endl;
  int erased_count = 0;
  vec2.erase(remove_if(vec2.begin(), vec2.end(),
                       [&erased_count](int i) {
                         if (erased_count >= 2)
                           return false;
                         else if (i % 2 == 1) {
                           ++erased_count;
                           return true;
                         }
                         return false;
                       }),
             vec2.end());  // also we can use comparer/comparison for pred
  Print(vec2.begin(), vec2.end());
  cout << "------------" << endl;

  // transform
  cout << "add 1 to all element" << endl;
  transform(vec2.begin(), vec2.end(), vec2.begin(),
            [](int i) { return i + 1; });
  Print(vec2.begin(), vec2.end());
  cout << "------------" << endl;

  // find
  auto current = vec2.begin();
  while (true) {
    current = find_if(current, vec2.end(), [](int i) { return i % 3 == 2; });
    if (current == vec2.end()) break;
    cout << "i%3 == 2 element: " << *current << endl;
    ++current;
  }
  cout << "------------" << endl;

  // all of, any of
  NationalPark park;
  park.AddUser("성삼문", 6);
  park.AddUser("하위지", 77);
  park.AddUser("이개", 41);
  park.AddUser("유성원", 38);

  cout << boolalpha;
  cout << "Can get old-group DC?: " << park.CanGetOldGroupDiscount() << endl;
  cout << "Can get child DC?: " << park.CanGetChildDiscount() << endl;
}
